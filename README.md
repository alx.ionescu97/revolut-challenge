# REVOLUT-CHALLENGE

### Summary

Implement an app which will do currency conversions by calling the following [api](https://revolut.duckdns.org/latest?base=EUR).

The app should update the course every 1 second. Touching one list element should make it the new base currency.

### Implementation

This project uses RxJava 2 for communication between the data model and presenter layers.

Networking is done using [Retrofit](https://square.github.io/retrofit/) and its reactive bindings.
HTTP responses are mapped by an RxJava 2 function called `FetchResponseMapper`.

Image loading is handled by [Glide4](https://github.com/bumptech/glide)

View binding is done by [ButterKnife](https://jakewharton.github.io/butterknife/)

Most of the apps' modules are provided by their respectively named classes (`DatabaseModule`, `PrefsModule`, `RemoteModule`) and are injected using [Dagger2](https://github.com/google/dagger) as needed.

The data layer `DataRepository` exposes RxJava 2 streams as a way of retrieving the most up-to-date course available. In order to do so, I used [ReactiveNetwork](https://github.com/pwittchen/ReactiveNetwork) which provides an easy to integrate way with the existing data streams:

```java
public Single<List<Currency>> getLatestCourse() {
    return ReactiveNetwork.checkInternetConnectivity()
            .flatMap((Function<Boolean, SingleSource<List<Currency>>>) available -> {
                if (available) {
                    return revolutApi.fetch(prefsRepository.getBaseCurrency())
                            .map(FetchResponseMapper.instance())
                            .map(RxCurrencySort.instance())
                            .onErrorResumeNext(readCache());
                } else {
                    return readCache();
                }
            });
    }

private Single<List<Currency>> readCache() {
    return databaseRepository.getAllCurrencies(prefsRepository.getBaseCurrency())
            .map(RxCurrencySort.instance());
}
```

If the network is available, then we retrieve the list of currency courses from the api or otherwise from the local NoSQL cache handled by [RxPaper2](https://github.com/pakoito/RxPaper2).

There are also methods exposed to store new courses to the cache:

```java
public Completable cacheLatestCourses(List<Currency> latest) {
    return this.databaseRepository.addCurrencies(latest)
            .onErrorComplete(throwable -> {
                return true;
            })
            .repeat(3);
}
```

### Downloads

"Stable" builds are available on this [page](https://gitlab.com/alx.ionescu97/revolut-challenge/pipelines?scope=branches&page=1), by downloading the latest build artifacts from the `develop` branch. 