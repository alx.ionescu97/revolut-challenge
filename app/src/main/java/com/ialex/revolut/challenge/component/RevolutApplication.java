package com.ialex.revolut.challenge.component;

import androidx.appcompat.app.AppCompatDelegate;

import com.ialex.revolut.challenge.BuildConfig;
import com.ialex.revolut.challenge.data.DataModule;
import com.ialex.revolut.challenge.injection.AppComponent;
import com.ialex.revolut.challenge.injection.AppModule;
import com.ialex.revolut.challenge.injection.DaggerAppComponent;
import com.ialex.revolut.challenge.util.DummyTree;
import com.pacoworks.rxpaper2.RxPaperBook;

import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;
import timber.log.Timber;

public class RevolutApplication extends BaseApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        RxPaperBook.init(this);
        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof UndeliverableException) {
                Timber.wtf(e, "Most likely a PaperDb issue");
            }
        });

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new DummyTree());
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule(getApplicationContext()))
                .build();
        appComponent.inject(this);
    }

    public static AppComponent component() {
        return ((RevolutApplication) getContext()).appComponent;
    }


}
