package com.ialex.revolut.challenge.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ialex.revolut.challenge.data.local.database.DatabaseRepository;
import com.ialex.revolut.challenge.data.local.prefs.PrefsRepository;
import com.ialex.revolut.challenge.data.remote.RevolutApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    private final Context appContext;

    public DataModule(Context context) {
        this.appContext = context;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd");

        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    DataRepository provideDataManager(RevolutApi stickersApi, PrefsRepository prefsRepository,
                                      DatabaseRepository databaseRepository) {
        return new DataRepository(appContext, stickersApi, prefsRepository, databaseRepository);
    }
}
