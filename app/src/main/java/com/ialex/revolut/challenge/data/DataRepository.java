package com.ialex.revolut.challenge.data;

import android.content.Context;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.ialex.revolut.challenge.data.local.RxCurrencySort;
import com.ialex.revolut.challenge.data.local.database.DatabaseRepository;
import com.ialex.revolut.challenge.data.local.prefs.PrefsRepository;
import com.ialex.revolut.challenge.data.remote.RevolutApi;
import com.ialex.revolut.challenge.data.remote.mapper.FetchResponseMapper;
import com.ialex.revolut.challenge.model.Currency;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import timber.log.Timber;


/**
 * The main data layer of the app. It aggregates the network layer with the cache containing
 * methods for both
 */
public class DataRepository {

    private final RevolutApi revolutApi;
    private final PrefsRepository prefsRepository;
    private final DatabaseRepository databaseRepository;
    private final Context context;

    DataRepository(Context context, RevolutApi revolutApi, PrefsRepository prefsRepository,
                          DatabaseRepository databaseRepository) {
        this.context = context;
        this.revolutApi = revolutApi;
        this.prefsRepository = prefsRepository;
        this.databaseRepository = databaseRepository;
    }

    /**
     * Method which will emit a single list of currencies either from the network api either from
     * the local no-sql cache.
     *
     * @return a single emitting the list of currencies
     */
    public Single<List<Currency>> getLatestCourse() {
        return ReactiveNetwork.checkInternetConnectivity()
                .flatMap((Function<Boolean, SingleSource<List<Currency>>>) available -> {
                    if (available) {
                        return revolutApi.fetch(prefsRepository.getBaseCurrency())
                                .map(FetchResponseMapper.instance())
                                .map(RxCurrencySort.instance())
                                .onErrorResumeNext(readCache());
                    } else {
                        return readCache();
                    }
                });
    }

    /**
     * Stores the given list of currencies to the local no-sql database
     *
     * @param latest the list that the adapter is currently displaying
     * @return a completable operation
     */
    public Completable cacheLatestCourses(List<Currency> latest) {
        // due to a known bug in android, the paper lib might get a null data directory
        // and the first call (when app starts for the first time) to the cache will fail.
        // retry it once more as the lib creator recommends

        return this.databaseRepository.addCurrencies(latest)
                .onErrorComplete(throwable -> {
                    Timber.e("Failure when caching");
                    return true;
                })
                .repeat(3);
    }

    /**
     * An observable which will emit boolean values representing network state
     *
     * @return observable of booleans
     */
    public Observable<Connectivity> watchNetworkConnectivity() {
        return ReactiveNetwork
                .observeNetworkConnectivity(context)
                .debounce(300, TimeUnit.MILLISECONDS);
    }

    private Single<List<Currency>> readCache() {
        return databaseRepository.getAllCurrencies(prefsRepository.getBaseCurrency())
                .map(RxCurrencySort.instance());
    }
}
