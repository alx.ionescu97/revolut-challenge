package com.ialex.revolut.challenge.data.local;

import com.ialex.revolut.challenge.model.Currency;

import java.util.Iterator;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * A mapping class which takes care of retrieving the new base from the cached list of currencies.
 * It tries to calculate the course for the old base and sets the course for the new base to 0F
 */
public class RxCurrencyMapper implements Function<List<Currency>, List<Currency>> {
    private static volatile RxCurrencyMapper instance;

    private volatile String mBaseCurrency;

    public static RxCurrencyMapper instance(String baseCurrency) {
        if (instance == null)
            instance = new RxCurrencyMapper();

        instance.setBaseCurrency(baseCurrency);
        return instance;
    }

    private RxCurrencyMapper() {

    }

    private void setBaseCurrency(String baseCurrency) {
        this.mBaseCurrency = baseCurrency;
    }

    @Override
    public List<Currency> apply(List<Currency> currencies) {
        if (currencies.isEmpty()) {
            return currencies;
        }

        Currency oldBase = null;

        for (Currency currency : currencies) {
            if (currency.isBase()) {
                oldBase = currency;
                break;
            }
        }

        Currency newBase = null;

        Iterator<Currency> it = currencies.iterator();

        while (it.hasNext()) {
            Currency currency = it.next();

            if (currency.getCurrencyLabel().equals(mBaseCurrency)) {
                newBase = currency;
                it.remove();
                break;
            }
        }

        if (newBase == null) {
            throw new RuntimeException("New base currency not found under current list");
        }

        if (oldBase == null) {
            newBase.makeBase();

            currencies.add(newBase);
            return currencies;
        }

        // iterate over the remaining currencies which need to be converted to the new course
        for (Currency other : currencies) {
            float newOldCourse = other.getValue() / newBase.getValue();
            other.setCourse(newOldCourse);
        }

        // set new currency course to 0
        newBase.makeBase();

        currencies.add(newBase);
        return currencies;
    }
}
