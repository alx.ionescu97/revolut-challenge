package com.ialex.revolut.challenge.data.local;

import com.ialex.revolut.challenge.model.Currency;

import java.util.Collections;
import java.util.List;

import io.reactivex.functions.Function;

public class RxCurrencySort implements Function<List<Currency>, List<Currency>> {

    private static RxCurrencySort sInstance;

    public static RxCurrencySort instance() {
        if (sInstance == null) {
            sInstance = new RxCurrencySort();
        }

        return sInstance;
    }

    private RxCurrencySort() {

    }

    @Override
    public List<Currency> apply(List<Currency> currencies) {
        Collections.sort(currencies, (lhs, rhs) -> {
            if (lhs.getCourse() == 0F) {
                return -1;
            } else if (rhs.getCourse() == 0F) {
                return 1;
            } else {
                return lhs.getCurrencyLabel().compareTo(rhs.getCurrencyLabel());
            }
        });

        return currencies;
    }
}