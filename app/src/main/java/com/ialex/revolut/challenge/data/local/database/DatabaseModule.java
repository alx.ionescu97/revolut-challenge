package com.ialex.revolut.challenge.data.local.database;

import com.pacoworks.rxpaper2.RxPaperBook;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    private final String BOOK_V2 = "book_v2";

    @Provides
    @Named("currencies_book")
    @Singleton
    RxPaperBook provideRxCurrencies() {
        return RxPaperBook.with(BOOK_V2 + "/currencies");
    }


    @Provides
    @Singleton
    DatabaseRepository provideDatabaseRepository(@Named("currencies_book") RxPaperBook rxDatabase) {
        return new DatabaseRepository(rxDatabase);
    }
}
