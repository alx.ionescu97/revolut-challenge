package com.ialex.revolut.challenge.data.local.database;

import com.ialex.revolut.challenge.data.local.RxCurrencyMapper;
import com.ialex.revolut.challenge.model.Currency;
import com.pacoworks.rxpaper2.RxPaperBook;

import org.reactivestreams.Publisher;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;

public class DatabaseRepository {

    private final RxPaperBook mCurrenciesBook;

    DatabaseRepository(RxPaperBook rxCurrencies) {
        this.mCurrenciesBook = rxCurrencies;
    }

    private Completable addCurrency(Currency currency) {
        return mCurrenciesBook.write(currency.getCurrencyLabel(), currency);
    }

    public Completable addCurrencies(List<Currency> currencies) {
        return Flowable.fromIterable(currencies)
                .flatMapCompletable(this::addCurrency);
    }

    public Completable deleteAllCurrencies() {
        return mCurrenciesBook.keys()
                .flatMapPublisher((Function<List<String>, Publisher<String>>) Flowable::fromIterable)
                .flatMapCompletable(mCurrenciesBook::delete);
    }

    public Single<List<Currency>> getAllCurrencies(String baseCurrency) {
        return mCurrenciesBook.keys()
                .flatMapPublisher((Function<List<String>, Publisher<String>>) Flowable::fromIterable)
                .filter(key -> !key.endsWith(".bak")) // filter out those keys, no idea where they come from
                .flatMapSingle((Function<String, SingleSource<Currency>>) mCurrenciesBook::read)
                .toList()
                .map(RxCurrencyMapper.instance(baseCurrency));
    }
}
