package com.ialex.revolut.challenge.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.ialex.revolut.challenge.data.local.prefs.util.StringPreference;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PrefsModule {

    @Provides
    @Singleton
    PrefsRepository providePrefsRepository() {
        return new PrefsRepository();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("prefs_utils", Context.MODE_PRIVATE);
    }

    @Provides
    @Named("base_currency")
    @Singleton
    StringPreference provideBaseCurrency(SharedPreferences preferences) {
        return new StringPreference(preferences, "base_currency", "EUR");
    }
}
