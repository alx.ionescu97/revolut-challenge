package com.ialex.revolut.challenge.data.local.prefs;


import com.ialex.revolut.challenge.component.RevolutApplication;
import com.ialex.revolut.challenge.data.local.prefs.util.StringPreference;

import javax.inject.Inject;
import javax.inject.Named;

public class PrefsRepository {

    @Inject
    @Named("base_currency")
    StringPreference mBaseCurrencyPref;

    public PrefsRepository() {
        RevolutApplication.component().inject(this);
    }

    public void setBaseCurrency(String newBase) {
        this.mBaseCurrencyPref.setImmediately(newBase);
    }

    public String getBaseCurrency() {
        return this.mBaseCurrencyPref.get();
    }
}
