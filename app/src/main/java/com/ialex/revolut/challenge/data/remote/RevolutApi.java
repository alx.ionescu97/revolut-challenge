package com.ialex.revolut.challenge.data.remote;

import com.ialex.revolut.challenge.data.remote.response.FetchResponse;

import io.reactivex.Single;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface RevolutApi {
    String BASE_URL = "https://revolut.duckdns.org";

    @GET("/latest")
    @Headers("User-Agent: android")
    Single<FetchResponse> fetch(@Query("base") String baseCurrency);
}
