package com.ialex.revolut.challenge.data.remote.mapper;

import com.ialex.revolut.challenge.data.Constants;
import com.ialex.revolut.challenge.data.remote.response.FetchResponse;
import com.ialex.revolut.challenge.data.remote.response.RatesResponse;
import com.ialex.revolut.challenge.model.Currency;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.functions.Function;
import timber.log.Timber;

public class FetchResponseMapper implements Function<FetchResponse, List<Currency>> {

    private static final String METHOD_FORMAT = "get%s";

    private final Map<String, String> mCurrencyToFlag = new HashMap<>();

    private static volatile FetchResponseMapper instance;

    public static FetchResponseMapper instance() {
        if (instance == null)
            instance = new FetchResponseMapper();
        return instance;
    }

    private FetchResponseMapper() {
        mCurrencyToFlag.put("EUR", "EU");
        mCurrencyToFlag.put("AUD", "AU");
        mCurrencyToFlag.put("BGN", "BG");
        mCurrencyToFlag.put("BRL", "BR");
        mCurrencyToFlag.put("CAD", "CA");
        mCurrencyToFlag.put("CHF", "CH");
        mCurrencyToFlag.put("CNY", "CN");
        mCurrencyToFlag.put("CZK", "CZ");
        mCurrencyToFlag.put("DKK", "DK");
        mCurrencyToFlag.put("GBP", "GB");
        mCurrencyToFlag.put("HKD", "HK");
        mCurrencyToFlag.put("HRK", "HR");
        mCurrencyToFlag.put("HUF", "HU");
        mCurrencyToFlag.put("IDR", "ID");
        mCurrencyToFlag.put("ILS", "IL");
        mCurrencyToFlag.put("INR", "IN");
        mCurrencyToFlag.put("ISK", "IS");
        mCurrencyToFlag.put("JPY", "JP");
        mCurrencyToFlag.put("KRW", "KR");
        mCurrencyToFlag.put("MXN", "MX");
        mCurrencyToFlag.put("MYR", "MY");
        mCurrencyToFlag.put("NOK", "NO");
        mCurrencyToFlag.put("NZD", "NZ");
        mCurrencyToFlag.put("PHP", "PH");
        mCurrencyToFlag.put("PLN", "PL");
        mCurrencyToFlag.put("RON", "RO");
        mCurrencyToFlag.put("RUB", "RU");
        mCurrencyToFlag.put("SEK", "SE");
        mCurrencyToFlag.put("SGD", "SG");
        mCurrencyToFlag.put("THB", "TH");
        mCurrencyToFlag.put("TRY", "TR");
        mCurrencyToFlag.put("USD", "US");
        mCurrencyToFlag.put("ZAR", "ZA");
    }

    @Override
    public List<Currency> apply(FetchResponse fetchResponse) {
        List<Currency> result = new ArrayList<>(32);
        RatesResponse rates = fetchResponse.getRates();

//        result.add(new Currency("EUR", mCurrencyToFlag.get("EUR"), rates.getEUR()));
//        result.add(new Currency("AUD", mCurrencyToFlag.get("AUD"), rates.getAUD()));
//        result.add(new Currency("BGN", mCurrencyToFlag.get("BGN"), rates.getBGN()));
//        result.add(new Currency("BRL", mCurrencyToFlag.get("BRL"), rates.getBRL()));
//        result.add(new Currency("CAD", mCurrencyToFlag.get("CAD"), rates.getCAD()));
//        result.add(new Currency("CHF", mCurrencyToFlag.get("CHF"), rates.getCHF()));
//        result.add(new Currency("CNY", mCurrencyToFlag.get("CNY"), rates.getCNY()));
//        result.add(new Currency("CZK", mCurrencyToFlag.get("CZK"), rates.getCZK()));
//        result.add(new Currency("DKK", mCurrencyToFlag.get("DKK"), rates.getDKK()));
//        result.add(new Currency("GBP", mCurrencyToFlag.get("GBP"), rates.getGBP()));
//        result.add(new Currency("HKD", mCurrencyToFlag.get("HKD"), rates.getHKD()));
//        result.add(new Currency("HRK", mCurrencyToFlag.get("HRK"), rates.getHRK()));
//        result.add(new Currency("HUF", mCurrencyToFlag.get("HUF"), rates.getHUF()));
//        result.add(new Currency("IDR", mCurrencyToFlag.get("IDR"), rates.getIDR()));
//        result.add(new Currency("ILS", mCurrencyToFlag.get("ILS"), rates.getILS()));
//        result.add(new Currency("INR", mCurrencyToFlag.get("INR"), rates.getINR()));
//        result.add(new Currency("ISK", mCurrencyToFlag.get("ISK"), rates.getISK()));
//        result.add(new Currency("JPY", mCurrencyToFlag.get("JPY"), rates.getJPY()));
//        result.add(new Currency("KRW", mCurrencyToFlag.get("KRW"), rates.getKRW()));
//        result.add(new Currency("MXN", mCurrencyToFlag.get("MXN"), rates.getMXN()));
//        result.add(new Currency("MYR", mCurrencyToFlag.get("MYR"), rates.getMYR()));
//        result.add(new Currency("NOK", mCurrencyToFlag.get("NOK"), rates.getNOK()));
//        result.add(new Currency("NZD", mCurrencyToFlag.get("NZD"), rates.getNZD()));
//        result.add(new Currency("PHP", mCurrencyToFlag.get("PHP"), rates.getPHP()));
//        result.add(new Currency("PLN", mCurrencyToFlag.get("PLN"), rates.getPLN()));
//        result.add(new Currency("RON", mCurrencyToFlag.get("RON"), rates.getRON()));
//        result.add(new Currency("RUB", mCurrencyToFlag.get("RUB"), rates.getRUB()));
//        result.add(new Currency("SEK", mCurrencyToFlag.get("SEK"), rates.getSEK()));
//        result.add(new Currency("SGD", mCurrencyToFlag.get("SGD"), rates.getSGD()));
//        result.add(new Currency("THB", mCurrencyToFlag.get("THB"), rates.getTHB()));
//        result.add(new Currency("TRY", mCurrencyToFlag.get("TRY"), rates.getTRY()));
//        result.add(new Currency("USD", mCurrencyToFlag.get("USD"), rates.getUSD()));
//        result.add(new Currency("ZAR", mCurrencyToFlag.get("ZAR"), rates.getZAR()));
        for (int i = 0; i < Constants.ISO_CODES.length; i++) {
            String code = Constants.ISO_CODES[i];

            Method method = null;
            try {
                method = rates.getClass().getMethod(String.format(METHOD_FORMAT, code));
            } catch (SecurityException e) {
                Timber.e(e);
            } catch (NoSuchMethodException e) {
                Timber.e(e);
            }

            if (method == null) {
                continue;
            }

            try {
                Object r = method.invoke(rates);

//                Currency current = mCachedCurrencies.get(i);

                if (r instanceof Float) {
                    float value = (float) r;

                    Currency current = new Currency();
                    current.setCourse(value);
                    current.setCurrencyLabel(code);

                    result.add(current);
                }
            } catch (IllegalArgumentException e) {
                Timber.e(e);
            } catch (IllegalAccessException e) {
                Timber.e(e);
            } catch (InvocationTargetException e) {
                Timber.e(e);
            }
        }

        return result;
    }
}
