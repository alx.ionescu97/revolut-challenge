package com.ialex.revolut.challenge.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchResponse {

    @SerializedName("base")
    @Expose
    private String baseCurrency;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("rates")
    @Expose
    private RatesResponse rates;

    public FetchResponse() {
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public String getDate() {
        return date;
    }

    public RatesResponse getRates() {
        return rates;
    }
}
