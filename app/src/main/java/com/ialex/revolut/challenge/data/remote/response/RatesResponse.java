package com.ialex.revolut.challenge.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatesResponse {

    @SerializedName("AUD")
    @Expose
    private float AUD;

    @SerializedName("BGN")
    @Expose
    private float BGN;

    @SerializedName("BRL")
    @Expose
    private float BRL;

    @SerializedName("CAD")
    @Expose
    private float CAD;

    @SerializedName("CHF")
    @Expose
    private float CHF;

    @SerializedName("CNY")
    @Expose
    private float CNY;

    @SerializedName("CZK")
    @Expose
    private float CZK;

    @SerializedName("DKK")
    @Expose
    private float DKK;

    @SerializedName("GBP")
    @Expose
    private float GBP;

    @SerializedName("HKD")
    @Expose
    private float HKD;

    @SerializedName("HRK")
    @Expose
    private float HRK;

    @SerializedName("HUF")
    @Expose
    private float HUF;

    @SerializedName("IDR")
    @Expose
    private float IDR;

    @SerializedName("ILS")
    @Expose
    private float ILS;

    @SerializedName("INR")
    @Expose
    private float INR;

    @SerializedName("ISK")
    @Expose
    private float ISK;

    @SerializedName("JPY")
    @Expose
    private float JPY;

    @SerializedName("KRW")
    @Expose
    private float KRW;

    @SerializedName("MXN")
    @Expose
    private float MXN;

    @SerializedName("MYR")
    @Expose
    private float MYR;

    @SerializedName("NOK")
    @Expose
    private float NOK;

    @SerializedName("NZD")
    @Expose
    private float NZD;

    @SerializedName("PHP")
    @Expose
    private float PHP;

    @SerializedName("PLN")
    @Expose
    private float PLN;

    @SerializedName("RON")
    @Expose
    private float RON;

    @SerializedName("RUB")
    @Expose
    private float RUB;

    @SerializedName("SEK")
    @Expose
    private float SEK;

    @SerializedName("SGD")
    @Expose
    private float SGD;

    @SerializedName("THB")
    @Expose
    private float THB;

    @SerializedName("TRY")
    @Expose
    private float TRY;

    @SerializedName("USD")
    @Expose
    private float USD;

    @SerializedName("ZAR")
    @Expose
    private float ZAR;

    @SerializedName("EUR")
    @Expose
    private float EUR;

    public RatesResponse() {
    }

    public float getAUD() {
        return AUD;
    }

    public float getBGN() {
        return BGN;
    }

    public float getBRL() {
        return BRL;
    }

    public float getCAD() {
        return CAD;
    }

    public float getCHF() {
        return CHF;
    }

    public float getCNY() {
        return CNY;
    }

    public float getCZK() {
        return CZK;
    }

    public float getDKK() {
        return DKK;
    }

    public float getGBP() {
        return GBP;
    }

    public float getHKD() {
        return HKD;
    }

    public float getHRK() {
        return HRK;
    }

    public float getHUF() {
        return HUF;
    }

    public float getIDR() {
        return IDR;
    }

    public float getILS() {
        return ILS;
    }

    public float getINR() {
        return INR;
    }

    public float getISK() {
        return ISK;
    }

    public float getJPY() {
        return JPY;
    }

    public float getKRW() {
        return KRW;
    }

    public float getMXN() {
        return MXN;
    }

    public float getMYR() {
        return MYR;
    }

    public float getNOK() {
        return NOK;
    }

    public float getNZD() {
        return NZD;
    }

    public float getPHP() {
        return PHP;
    }

    public float getPLN() {
        return PLN;
    }

    public float getRON() {
        return RON;
    }

    public float getRUB() {
        return RUB;
    }

    public float getSEK() {
        return SEK;
    }

    public float getSGD() {
        return SGD;
    }

    public float getTHB() {
        return THB;
    }

    public float getTRY() {
        return TRY;
    }

    public float getUSD() {
        return USD;
    }

    public float getZAR() {
        return ZAR;
    }

    public float getEUR() {
        return EUR;
    }
}
