package com.ialex.revolut.challenge.injection;

import android.content.Context;

import com.ialex.revolut.challenge.component.RevolutApplication;
import com.ialex.revolut.challenge.data.DataModule;
import com.ialex.revolut.challenge.data.DataRepository;
import com.ialex.revolut.challenge.data.local.database.DatabaseModule;
import com.ialex.revolut.challenge.data.local.database.DatabaseRepository;
import com.ialex.revolut.challenge.data.local.prefs.PrefsModule;
import com.ialex.revolut.challenge.data.local.prefs.PrefsRepository;
import com.ialex.revolut.challenge.data.local.prefs.util.StringPreference;
import com.ialex.revolut.challenge.data.remote.RemoteModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DataModule.class, RemoteModule.class, DatabaseModule.class, PrefsModule.class})
public interface AppComponent {

    void inject(RevolutApplication application);

    void inject(PrefsRepository prefsRepository);

    DataRepository dataManager();

    DatabaseRepository databaseRepository();

    PrefsRepository prefsRepository();

    Context context();

    @Named("base_currency")
    StringPreference baseCurrencyPref();
}
