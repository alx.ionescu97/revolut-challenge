package com.ialex.revolut.challenge.model;

public class Currency {

    public static final String KEY_COURSE = "key_course";
    public static final String KEY_VALUE = "key_value";
    public static final String KEY_ISO_CODE = "key_iso_code";

    private String mCurrencyLabel;

    private float mCourse;

    private float mValue;

    public Currency(String mCurrencyLabel, float mCourse) {
        this.mCurrencyLabel = mCurrencyLabel;
        this.mCourse = mCourse;
    }

    public Currency() {

    }

    public boolean isBase() {
        return (this.mCourse == 0F);
    }

    public float getValue() {
        return mValue;
    }

    /**
     * Calculates the new value for this currency based on baseAmount param
     *
     * @param baseAmount the amount of units of base currency
     */
    public void setValue(float baseAmount) {
        if (isBase()) {
            this.mValue = baseAmount;
        } else {
            this.mValue = mCourse * baseAmount;
        }
    }

    public String getCurrencyLabel() {
        return mCurrencyLabel;
    }

    public float getCourse() {
        return mCourse;
    }

    public void setCurrencyLabel(String currencyLabel) {
        this.mCurrencyLabel = currencyLabel;
    }

    public void setCourse(float course) {
        this.mCourse = course;
    }

    public void makeBase() {
        this.mCourse = 0F;
    }
}
