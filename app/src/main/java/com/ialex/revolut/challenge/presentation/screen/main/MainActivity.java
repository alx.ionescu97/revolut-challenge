package com.ialex.revolut.challenge.presentation.screen.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ialex.revolut.challenge.R;
import com.ialex.revolut.challenge.component.RevolutApplication;
import com.ialex.revolut.challenge.model.Currency;
import com.ialex.revolut.challenge.presentation.screen.main.recyclerview.CurrenciesAdapter;
import com.ialex.revolut.challenge.presentation.screen.main.recyclerview.CurrenciesListener;
import com.ialex.revolut.challenge.presentation.util.NonNullPair;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements MainContract.View, CurrenciesListener {

    @Inject
    MainContract.Presenter presenter;

    @BindView(R.id.rv_currencies)
    RecyclerView mCurrenciesRecycle;

    @BindView(R.id.layout_no_internet)
    FrameLayout mLayoutNoInternet;

    private CurrenciesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        MainComponent component = DaggerMainComponent.builder()
                .appComponent(RevolutApplication.component())
                .mainModule(new MainModule())
                .build();
        component.inject(this);

        setup();
//        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.attachView(this);
        presenter.onResume();

        mAdapter.setListener(this);
        presenter.startDataStream(mAdapter.getItems());
    }

    @Override
    protected void onPause() {
        super.onPause();

        mAdapter.setListener(null);

        // this method will detach the view after it is done caching current items
        // do not explicitly call presenter.detachView() as it will prevent the caching from happening
        presenter.onPause();
        presenter.detachView();
    }

    @Override
    public void onReceiveNewRates(NonNullPair<List<Currency>, DiffUtil.DiffResult> pair) {
        presenter.storeToCache(pair.getFirst());
        mAdapter.accept(pair);
    }

    @Override
    public void onFailureNewRates(String error) {

    }

    @Override
    public void onNetworkAvailable() {
        Timber.d("Network became available");
        mLayoutNoInternet.animate().alpha(0F).setDuration(300).start();
    }

    @Override
    public void onNetworkUnavailable() {
        Timber.d("Network became unavailable");
        mLayoutNoInternet.animate().alpha(1F).setDuration(300).start();
    }

    @Override
    public CurrenciesAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void onSelectBaseCurrency(Currency newBase) {
        Timber.d("Newly selected base currency: %s", newBase.getCurrencyLabel());
        presenter.onBaseCurrencySelected(newBase);
    }

    @Override
    public void onBaseCurrencyLostFocus() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm == null) {
            Timber.e("Null input method manager service");
            return;
        }

        RecyclerView.LayoutManager manager = mCurrenciesRecycle.getLayoutManager();
        if (manager instanceof LinearLayoutManager) {
            int firstVisiblePosition = ((LinearLayoutManager) manager).findFirstVisibleItemPosition();

            // hide soft keyboard if the base item is no longer visible
            if (firstVisiblePosition > 0) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        }
    }

    private void setup() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mCurrenciesRecycle.setLayoutManager(llm);
        mCurrenciesRecycle.setHasFixedSize(true);

        mAdapter = new CurrenciesAdapter(this);
        mCurrenciesRecycle.setAdapter(mAdapter);
    }
}
