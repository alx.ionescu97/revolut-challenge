package com.ialex.revolut.challenge.presentation.screen.main;

import com.ialex.revolut.challenge.injection.AppComponent;
import com.ialex.revolut.challenge.injection.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = {MainModule.class})
public interface MainComponent extends AppComponent {

    void inject(MainActivity activity);

    MainContract.Presenter presenter();
}
