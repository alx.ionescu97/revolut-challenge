package com.ialex.revolut.challenge.presentation.screen.main;

import androidx.recyclerview.widget.DiffUtil;

import com.ialex.revolut.challenge.model.Currency;
import com.ialex.revolut.challenge.presentation.util.NonNullPair;

import java.util.List;

public interface MainContract {

    interface View {

        void onReceiveNewRates(NonNullPair<List<Currency>, DiffUtil.DiffResult> pair);

        void onFailureNewRates(String error);

        void onNetworkAvailable();

        void onNetworkUnavailable();
    }

    interface Presenter extends com.ialex.revolut.challenge.presentation.util.Presenter<View> {

        void onResume();

        void onPause();

        void storeToCache(List<Currency> current);

        void onBaseCurrencySelected(Currency base);

        void startDataStream(List<Currency> oldList);
    }
}
