package com.ialex.revolut.challenge.presentation.screen.main;

import com.ialex.revolut.challenge.data.DataRepository;
import com.ialex.revolut.challenge.data.local.prefs.PrefsRepository;
import com.ialex.revolut.challenge.injection.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    @Provides
    @PerActivity
    MainContract.Presenter providePresenter(PrefsRepository prefsRepository,
                                            DataRepository dataRepository) {
        return new MainPresenter(prefsRepository, dataRepository);
    }
}
