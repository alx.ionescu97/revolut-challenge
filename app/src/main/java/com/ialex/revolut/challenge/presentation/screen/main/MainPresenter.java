package com.ialex.revolut.challenge.presentation.screen.main;

import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.ialex.revolut.challenge.data.DataRepository;
import com.ialex.revolut.challenge.data.local.prefs.PrefsRepository;
import com.ialex.revolut.challenge.model.Currency;
import com.ialex.revolut.challenge.presentation.screen.main.recyclerview.CurrenciesDiffCallback;
import com.ialex.revolut.challenge.presentation.util.BaseRxPresenter;
import com.ialex.revolut.challenge.presentation.util.NonNullPair;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class MainPresenter extends BaseRxPresenter<MainContract.View> implements MainContract.Presenter {

    private PrefsRepository prefsRepository;
    private DataRepository dataRepository;

    private PublishSubject<Long> mBaseChangedSubject;
    private PublishSubject<List<Currency>> mCacheSubject;

    MainPresenter(PrefsRepository prefsRepository, DataRepository dataRepository) {
        this.prefsRepository = prefsRepository;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onResume() {
        Observable<Connectivity> conn = dataRepository.watchNetworkConnectivity();

        subscribe(conn, connectivity -> {
            if (connectivity.available()) {
                view.onNetworkAvailable();
            } else {
                view.onNetworkUnavailable();
            }
        }, Timber::e, () -> Timber.d("Network observing completed"));

        mBaseChangedSubject = PublishSubject.create();
        mCacheSubject = PublishSubject.create();

        Completable cache = mCacheSubject.toFlowable(BackpressureStrategy.LATEST)
                .observeOn(Schedulers.computation())
                .flatMapCompletable(currencies -> dataRepository.cacheLatestCourses(currencies));

        subscribe(cache,
                () -> Timber.d("Cached items"),
                throwable -> Timber.e(throwable, "Failure when caching"));
    }

    @Override
    public void onPause() {
        mBaseChangedSubject.onComplete();
        mBaseChangedSubject = null;
        mCacheSubject.onComplete();
        mCacheSubject = null;
    }

    @Override
    public void storeToCache(List<Currency> current) {
        mCacheSubject.onNext(current);
    }

    @Override
    public void onBaseCurrencySelected(Currency base) {
        prefsRepository.setBaseCurrency(base.getCurrencyLabel());
        // emit a number to trigger the base currency update
        mBaseChangedSubject.onNext(1000L);
    }

    @Override
    public void startDataStream(List<Currency> initialListOfCurrencies) {
        Flowable<Long> fixedUpdates = Flowable.interval(0, 1, TimeUnit.SECONDS);
        Flowable<Long> forcedUpdates = mBaseChangedSubject.toFlowable(BackpressureStrategy.LATEST)
                .observeOn(Schedulers.computation());

        Flowable<NonNullPair<List<Currency>, DiffUtil.DiffResult>> updates =
                dataUpdates(initialListOfCurrencies, fixedUpdates, forcedUpdates);

        subscribeComputation(updates, pair -> {
            Timber.d("Got %d currencies", pair.getFirst().size());
            view.onReceiveNewRates(pair);
        }, throwable -> {
            Timber.e(throwable, "Failure getting currencies");
            view.onFailureNewRates("Api network call failed");
        });
    }

    /**
     * The main method which is responsible for retrieving data from the available repository
     * (network or local cache)
     * It mainly uses the scan operator to calculate a {@link DiffUtil.DiffResult} from the current
     * and previous list of currencies and then dispatches those updates on the main thread to the
     * adapter
     *
     * @param cache the list of currencies currently displayed by the recycler view
     *              used as initial item for the scan operator
     * @param fixedUpdates updates generated every second
     * @param forcedUpdates updates triggered by a change of the main currency
     *
     * @return data to be shown in the recycler adapter
     */
    private Flowable<NonNullPair<List<Currency>, DiffUtil.DiffResult>> dataUpdates(List<Currency> cache, Flowable<Long> fixedUpdates, Flowable<Long> forcedUpdates) {
        return Flowable.merge(fixedUpdates, forcedUpdates)
                .flatMapSingle((Function<Long, SingleSource<List<Currency>>>) idx -> {
                    Timber.d("Request no: %d", idx);
                    return dataRepository.getLatestCourse();
                })
                .scan(new NonNullPair<>(cache, CurrenciesDiffCallback.emptyResult(cache)), (oldPair, currencies) -> {
                    // here we calculate
                    Currency newBase = null;
                    for (Currency curr : currencies) {
                        if (curr.isBase()) {
                            newBase = curr;
                            break;
                        }
                    }

                    float oldBaseValue = getOldBaseValue(newBase, oldPair.getFirst());

                    // calculate values for the new list of currencies
                    for (Currency curr : currencies) {
                        curr.setValue(oldBaseValue);
                    }

                    // calculate difference to old
                    DiffUtil.Callback callback = new CurrenciesDiffCallback(oldPair.first, currencies);
                    DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);

                    return new NonNullPair<>(currencies, result);
                })
                .skip(1)
                .filter(pair -> !pair.getFirst().isEmpty());
    }

    private float getOldBaseValue(@Nullable Currency newBase, List<Currency> oldCurrencies) {
        if (newBase == null || oldCurrencies.isEmpty()) {
            return 1F;
        }

        for (Currency oldCurrency : oldCurrencies) {
            if (TextUtils.equals(oldCurrency.getCurrencyLabel(), newBase.getCurrencyLabel())) {
                return oldCurrency.getValue();
            }
        }

        return 1F;
    }
}
