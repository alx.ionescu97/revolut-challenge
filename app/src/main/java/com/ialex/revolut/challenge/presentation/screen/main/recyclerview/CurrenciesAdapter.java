package com.ialex.revolut.challenge.presentation.screen.main.recyclerview;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ialex.revolut.challenge.R;
import com.ialex.revolut.challenge.component.GlideApp;
import com.ialex.revolut.challenge.model.Currency;
import com.ialex.revolut.challenge.presentation.util.GenericRecyclerAdapter;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import timber.log.Timber;

public class CurrenciesAdapter extends GenericRecyclerAdapter<Currency, CurrenciesAdapter.ViewHolder> {

    private CurrenciesListener mListener;

    public CurrenciesAdapter(Context context) {
        super(context);
    }

    public void setListener(CurrenciesListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = mLayoutInflater.inflate(R.layout.item_currency, parent, false);
        return new ViewHolder(root, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mItems.get(position));
//        Timber.d("onBindViewHolder: %d", position);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if(payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
            return;
        }

        Bundle o = (Bundle) payloads.get(0);

        for (String key : o.keySet()) {
            switch (key) {
                case Currency.KEY_COURSE:
                    float newRate = o.getFloat(key, 0F);
                    holder.setCurrentRate(newRate);
                    break;
                case Currency.KEY_VALUE:
                    float newValue = o.getFloat(key, 1F);
                    holder.setCalculatedValue(newValue);
                    break;
                case Currency.KEY_ISO_CODE:
                    String newIsoCode = o.getString(key);

                    if (newIsoCode == null) {
                        continue;
                    }

                    holder.setCourseFlag(newIsoCode);
                    break;
            }
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private static final String FLAG_URL = "https://www.cardition.com/assets/flags/%s.png";

        @BindView(R.id.iv_flag)
        AppCompatImageView ivFlag;

        @BindView(R.id.et_current_value)
        AppCompatEditText etCurrentValue;

        @BindView(R.id.tv_current_rate)
        AppCompatTextView tvCurrentRate;

        @BindView(R.id.tv_currency_iso)
        AppCompatTextView tvCurrencyIso;

        @BindView(R.id.tv_currency_description)
        AppCompatTextView tvCurrencyDescription;

        private Currency mCurrentCurrency;
        private boolean mBlockCompletion;

        private final WeakReference<CurrenciesListener> mListenerRef;

        ViewHolder(@NonNull View itemView, CurrenciesListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.mListenerRef = new WeakReference<>(listener);
        }

        void bind(Currency current) {
            this.mCurrentCurrency = current;

            setCalculatedValue(current.getValue());
            setCurrentRate(current.getCourse());
            setCourseFlag(current.getCurrencyLabel());
            setCurrencyIso(current.getCurrencyLabel());
        }

        void setCalculatedValue(float value) {
            mBlockCompletion = true;
            etCurrentValue.setText(String.format(Locale.getDefault(), "%.2f", value));
            mBlockCompletion = false;
        }

        void setCurrentRate(float rate) {
            if (rate == 0F) {
                tvCurrentRate.setText(R.string.base_course);
            } else {
                tvCurrentRate.setText(String.format(Locale.getDefault(), "%.4f", rate));
            }
        }

        void setCurrencyIso(String symbol) {
            tvCurrencyIso.setText(symbol);
        }

        void setCourseFlag(String isoCode) {
            GlideApp.with(ivFlag)
                    .load(String.format(FLAG_URL, isoCode.toLowerCase()))
                    .circleCrop()
                    .into(ivFlag);

            tvCurrencyDescription.setText(java.util.Currency.getInstance(isoCode).getDisplayName(Locale.ENGLISH));
        }

        @OnFocusChange(R.id.et_current_value)
        void onSelectNewBaseCurrency(boolean focused) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) {
                return;
            }

            if (mListenerRef.get() == null) {
                return;
            }

            if (focused) {
                mListenerRef.get().onSelectBaseCurrency(mCurrentCurrency);
            } else {
                // if the view lost focus, check if it's the edit text corresponding to the base
                // in that case, notify the activity to hide the keyboard
                mListenerRef.get().onBaseCurrencyLostFocus();
            }
        }

        @OnTextChanged(R.id.et_current_value)
        void onBaseValueChanged(CharSequence cs) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION || mBlockCompletion) {
                return;
            }

            if (mListenerRef.get() == null) {
                return;
            }

            Timber.d("Text Changed pos: %d => %s", getAdapterPosition(), cs);

            float value = 0F;
            try {
                if (!TextUtils.isEmpty(cs)) {
                    value = Float.parseFloat(cs.toString());
                }
            } catch (NumberFormatException e) {
                Timber.e(e);
            }

            CurrenciesAdapter adapter = mListenerRef.get().getAdapter();
            for (int i = 0; i < adapter.getItems().size(); i++) {
                Currency currency = adapter.getItems().get(i);
                currency.setValue(value);

                if (i == position) {
                    continue;
                }

                adapter.notifyItemChanged(i, getChangePayload(currency.getValue()));
            }
        }

        private Bundle getChangePayload(float value) {
            Bundle diffBundle = new Bundle();
            diffBundle.putFloat(Currency.KEY_VALUE, value);
            return diffBundle;
        }
    }
}
