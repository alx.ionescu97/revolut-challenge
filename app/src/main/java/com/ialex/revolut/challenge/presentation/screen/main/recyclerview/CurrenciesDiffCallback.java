package com.ialex.revolut.challenge.presentation.screen.main.recyclerview;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.ialex.revolut.challenge.model.Currency;

import java.util.Collections;
import java.util.List;

public class CurrenciesDiffCallback extends DiffUtil.Callback {

    public static DiffUtil.DiffResult emptyResult(List<Currency> old) {
        return DiffUtil.calculateDiff(
                new CurrenciesDiffCallback(Collections.emptyList(), old),
                false);
    }

    private final List<Currency> mOldList;
    private final List<Currency> mNewList;

    public CurrenciesDiffCallback(List<Currency> oldList, List<Currency> newList) {
        this.mOldList = oldList;
        this.mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList == null ? 0 : mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList == null ? 0 : mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        String oldCardId = mOldList.get(oldItemPosition).getCurrencyLabel();
        String newCardId = mNewList.get(newItemPosition).getCurrencyLabel();

        if (oldCardId == null && newCardId == null) {
            return true;
        }

        if (oldCardId == null || newCardId == null) {
            return false;
        }

        return oldCardId.equals(newCardId);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        float oldCourse = mOldList.get(oldItemPosition).getCourse();
        float newCourse = mNewList.get(newItemPosition).getCourse();

        return Float.compare(oldCourse, newCourse) == 0;
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
//        return super.getChangePayload(oldItemPosition, newItemPosition);
        Currency oldCurrency = mOldList.get(oldItemPosition);
        Currency newCurrency = mNewList.get(newItemPosition);

        Bundle diffBundle = new Bundle();
        diffBundle.putFloat(Currency.KEY_COURSE, newCurrency.getCourse());
        diffBundle.putFloat(Currency.KEY_VALUE, newCurrency.getValue());

        if (!TextUtils.equals(oldCurrency.getCurrencyLabel(), newCurrency.getCurrencyLabel())) {
            diffBundle.putString(Currency.KEY_ISO_CODE, newCurrency.getCurrencyLabel());
        }

        return diffBundle;
    }
}