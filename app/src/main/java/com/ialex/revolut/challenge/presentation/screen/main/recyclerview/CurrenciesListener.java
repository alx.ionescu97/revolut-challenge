package com.ialex.revolut.challenge.presentation.screen.main.recyclerview;

import com.ialex.revolut.challenge.model.Currency;

public interface CurrenciesListener {

    CurrenciesAdapter getAdapter();

    void onSelectBaseCurrency(Currency newBase);

    void onBaseCurrencyLostFocus();
}
