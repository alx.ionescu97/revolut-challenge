package com.ialex.revolut.challenge.presentation.util;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseRxPresenter<V> extends BasePresenter<V> {

    private CompositeDisposable disposables;

    @Override
    public void detachView() {
        unsubscribe();
        super.detachView();
    }

    protected <D> void subscribe(Single<D> single, Consumer<D> onSuccess, Consumer<Throwable> onError) {
        if (disposables == null)
            disposables = new CompositeDisposable();

        disposables.add(single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSuccess, onError));
    }

    protected <D> void subscribe(Observable<D> observable, Consumer<D> onNext, Consumer<Throwable> onError, Action onComplete) {
        if (disposables == null)
            disposables = new CompositeDisposable();

        disposables.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError, onComplete));
    }

    protected void subscribe(Completable completable, Action onComplete, Consumer<Throwable> onError) {
        if (disposables == null)
            disposables = new CompositeDisposable();

        disposables.add(completable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError));
    }

    protected <D> void subscribe(Flowable<D> flowable, Consumer<D> onNext, Consumer<Throwable> onError) {
        if (disposables == null)
            disposables = new CompositeDisposable();

        disposables.add(flowable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError));
    }

    protected <D> void subscribeComputation(Flowable<D> flowable, Consumer<D> onNext, Consumer<Throwable> onError) {
        if (disposables == null)
            disposables = new CompositeDisposable();

        disposables.add(flowable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError));
    }

    protected void unsubscribe() {
        if (disposables != null && !disposables.isDisposed()) {
            disposables.clear();
            disposables.dispose();
        }
        disposables = null;
    }
}