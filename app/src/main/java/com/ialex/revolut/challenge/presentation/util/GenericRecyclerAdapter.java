package com.ialex.revolut.challenge.presentation.util;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    protected List<T> mItems;
    protected Context mContext;
    protected LayoutInflater mLayoutInflater;

    public GenericRecyclerAdapter(@NonNull Context context, List<T> initial) {
        this.mContext = context;
        this.mItems = initial;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    public GenericRecyclerAdapter(Context mContext) {
        this(mContext, new ArrayList<>());
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public List<T> getItems() {
        return mItems;
    }

    public void accept(NonNullPair<List<T>, DiffUtil.DiffResult> pair) {
        this.mItems = pair.getFirst();
        pair.getSecond().dispatchUpdatesTo(this);
    }
}
