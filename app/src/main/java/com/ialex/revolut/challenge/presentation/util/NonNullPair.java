package com.ialex.revolut.challenge.presentation.util;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

public class NonNullPair<F, S> extends Pair<F, S> {

    public NonNullPair(@NonNull F first, @NonNull S second) {
        super(first, second);
    }

    @NonNull
    public F getFirst() {
        return first;
    }

    @NonNull
    public S getSecond() {
        return second;
    }
}
