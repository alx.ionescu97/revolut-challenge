package com.ialex.revolut.challenge.presentation.util;

public interface Presenter<V> {

    void attachView(V view);

    void detachView();
}