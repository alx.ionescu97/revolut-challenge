package com.ialex.revolut.challenge.util;

import androidx.annotation.NonNull;

import timber.log.Timber;

public class DummyTree extends Timber.Tree {

    @Override
    protected void log(int priority, String tag, @NonNull String message, Throwable t) {
        // we should report to analytics severe errors here, but for now no implementation,
        // just swallow log messages
    }
}